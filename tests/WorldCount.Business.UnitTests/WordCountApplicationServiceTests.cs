﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using Castle.Core.Internal;
using FluentAssertions;
using Moq;
using WordCount.Business;
using WordCount.Business.Services;
using WordCount.Core;
using WordCount.Core.Interfaces;
using WordCount.Core.Parameters;
using Xunit;

namespace WorldCount.Business.UnitTests
{
    public class WordCountApplicationServiceTests
    {
        protected Fixture Fixture = new Fixture();
   
        protected Mock<ISearchProvider> Provider1;
        protected Mock<ISearchProvider> Provider2;
        protected List<ISearchProvider> SearchProviders;
        protected IWordCountApplicationService Sut;



        public WordCountApplicationServiceTests()
        {
            SearchProviders = new List<ISearchProvider>();
            Provider1 = new Mock<ISearchProvider>();
            Provider2 = new Mock<ISearchProvider>();

            Provider1.SetupGet(p => p.Name).Returns("Provider1");
            Provider2.SetupGet(p => p.Name).Returns("Provider2");

            SearchProviders.Add(Provider1.Object);
            SearchProviders.Add(Provider2.Object);

            Sut = new WordCountApplicationService(SearchProviders);
        }

        public class CountWordsAsync : WordCountApplicationServiceTests
        {
            [Theory]
            [ClassData(typeof(ValidData))]
            public async Task When_searching_with_multiple_providers_all_are_called_once_with_each_word(
                IEnumerable<string> words)
            {
                var extraSearchProvider = new Mock<ISearchProvider>();
                SetupProviders(1, 1);

                SearchProviders.Add(extraSearchProvider.Object);

                var res = await Sut.CountWordsAsync(words);

                var wordCount = words.IsNullOrEmpty() ? 0 : words.Count();

                foreach (var searchProvider in SearchProviders)
                    Mock.Get(searchProvider).Verify(p => p.CountWordAsync(It.IsAny<WordCountQuery>()),
                        Times.Exactly(wordCount));
            }


            private void SetupProviders(int returns1, int returns2)
            {
                Provider1.Setup(p => p.CountWordAsync(It.IsAny<WordCountQuery>()))
                    .ReturnsAsync(() => new WordCountResult("Provider1", Fixture.Create<string>(), returns1, true));


                Provider2.Setup(p => p.CountWordAsync(It.IsAny<WordCountQuery>()))
                    .ReturnsAsync(() => new WordCountResult("Provider2", Fixture.Create<string>(), returns2, true));
            }

            [Theory]
            [ClassData(typeof(ValidData))]
            public async Task When_words_are_found_the_result_is_summed_up_per_provider(IEnumerable<string> words)
            {
                var provider1Hits = 1;
                var provider2Hits = 2;
                SetupProviders(provider1Hits, provider2Hits);

                var res = await Sut.CountWordsAsync(words);

                if (words.IsNullOrEmpty())
                {
                    res.Results.Keys.Count.Should().Be(0);
                }
                else
                {
                    res.Results.Keys.Count.Should().Be(SearchProviders.Count);

                    res.Results.Values.Sum().Should().Be(provider1Hits * words.Count() + provider2Hits * words.Count());
                }
            }


            [Fact]
            public async Task When_search_string_is_empty_it_should_not_throw()
            {
                Func<Task> a = async () => await Sut.CountWordsAsync(Enumerable.Empty<string>());

                a.Should().NotThrow();
            }

            [Fact]
            public async Task When_search_string_is_null_it_should_throw()
            {
                Func<Task> a = async () => await Sut.CountWordsAsync(null);

                a.Should().Throw<ArgumentNullException>("data in is invalid");
            }
        }
    }
}