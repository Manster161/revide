﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace WorldCount.Business.UnitTests
{
    public class ValidData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
           
            yield return new object[]
            {
               Enumerable.Empty<string>()
            };
            yield return new object[]
            {
                new List<string>
                {
                    "one"
                }
            };
            yield return new object[]
            {
                new List<string>
                {
                    "one",
                    "two",
                    "three",
                }
            };
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }


}