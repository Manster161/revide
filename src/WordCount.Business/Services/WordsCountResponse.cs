﻿using System.Collections.Generic;

namespace WordCount.Business.Services
{
    public class WordsCountResponse
    {
        public IDictionary<string, int> Results { get; }

        public IEnumerable<string> Words { get; }

        private WordsCountResponse()
        {
            Results = new Dictionary<string, int>();
        }

        public WordsCountResponse(IEnumerable<string> words) : this()
        {
            Words = words;
        }
       
        public void AddResult(string provider, int count)
        {
            Results.Add(provider, count);
        }
    }
}