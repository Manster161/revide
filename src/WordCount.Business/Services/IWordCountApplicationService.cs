﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WordCount.Business.Services
{
    public interface IWordCountApplicationService
    {
         Task<WordsCountResponse> CountWordsAsync(IEnumerable<string> words);
    }
}
