﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WordCount.Core;
using WordCount.Core.Interfaces;
using WordCount.Core.Parameters;
using WorldCount.Utils;

namespace WordCount.Business.Services
{
    public class WordCountApplicationService : IWordCountApplicationService
    {
        private readonly IEnumerable<ISearchProvider> _searchProviders;

        public WordCountApplicationService(IEnumerable<ISearchProvider> providers)
        {
            _searchProviders = Guard.AgainstNull(providers, nameof(providers));
        }


        public async Task<WordsCountResponse> CountWordsAsync(IEnumerable<string> words)
        {
            var list = Guard.AgainstNull(words, nameof(words)).ToList();

            var ret = new WordsCountResponse(list);

            if (!list.Any()) return ret;

            var tasks = new List<Task<WordCountResult>>();

            foreach (var searchProvider in _searchProviders)
                tasks.AddRange(list.Select(word => searchProvider.CountWordAsync(new WordCountQuery(word))));

            await Task.WhenAll(tasks);

            foreach (var groupResult in
                tasks.Where(t =>
                        t.Result != null &&
                        t.Result.Success)
                    .GroupBy(
                        t => t.Result.Provider,
                        t => t.Result.Count,
                        (key, val) => new {Name = key, Count = val.Sum()}))
                ret.AddResult(groupResult.Name, groupResult.Count);

            return ret;
        }
    }
}