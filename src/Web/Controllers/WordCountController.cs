﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Services;
using WordCount.Infrastructure.SearchProviders.Entities.Bing;
using WorldCount.Parameters;
using WorldCount.Utils;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WordCountController : ControllerBase
    {
        private readonly IWordCountRestService _restService;

        public WordCountController(IWordCountRestService restService)
        {
            _restService = Guard.AgainstNull(restService, nameof(restService)) ;
        }
        // GET api/values
        [HttpGet]
        public async Task<WordCountSummaryResponse> Get([FromQuery] string q)
        {
            return await _restService.CountWords(CleanRequest(q));
        }

        private static WordCountSummaryRequest CleanRequest(string q)
        {
            var ret = new WordCountSummaryRequest(q.Trim().Split(' ').ToList().Distinct());
            return ret;
        }
    }
}
