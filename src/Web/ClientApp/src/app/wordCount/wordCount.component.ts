import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './wordCount.component.html',
})
export class WordCountComponent {
    public http : HttpClient; 
    public  query = "";
    public googleHits = "";
    public bingHits = "";
    constructor(http: HttpClient){
        this.http = http;
    }

    public startCounting(query: string){
       this.http.get('api/WordCount?q=' + query).subscribe(result => {
            this.bingHits = result["wordCount"].Bing;
            this.googleHits = result["wordCount"].Google;
          }, error => console.error(error));
        }
    }

