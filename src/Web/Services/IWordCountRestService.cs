﻿using System.Threading.Tasks;
using WorldCount.Parameters;

namespace Web.Services
{
    public interface IWordCountRestService
    {
        Task<WordCountSummaryResponse> CountWords(WordCountSummaryRequest request);
    }
}
