﻿using WordCount.Business.Services;
using WorldCount.Parameters;

namespace Web.Services
{
    public static class ParameterExtensions
    {
        public static WordCountSummaryResponse ToSummaryResponse(this WordsCountResponse from)
        {
            return new WordCountSummaryResponse(from.Words, from.Results);
        }
    }
}