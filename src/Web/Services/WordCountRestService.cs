﻿using System.Threading.Tasks;
using WordCount.Business.Services;
using WorldCount.Parameters;
using WorldCount.Utils;

namespace Web.Services
{
    class WordCountRestService : IWordCountRestService
    {
        private readonly IWordCountApplicationService _service;

        public WordCountRestService(IWordCountApplicationService service)
        {
            _service = Guard.AgainstNull(service, nameof(service));
        }
        public async Task<WordCountSummaryResponse> CountWords(WordCountSummaryRequest request)
        {
            var resp = await _service.CountWordsAsync(request.SearchWords);

            return resp.ToSummaryResponse();
        }
    }
}