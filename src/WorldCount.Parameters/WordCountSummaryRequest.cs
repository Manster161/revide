﻿using System.Collections.Generic;

namespace WorldCount.Parameters
{
    public class WordCountSummaryRequest
    {
        public IEnumerable<string> SearchWords { get; }

        public WordCountSummaryRequest(IEnumerable<string> searchWords)
        {
            SearchWords = searchWords;
        }
        
    }
}