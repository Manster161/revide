﻿using System.Collections.Generic;
using System.Linq;

namespace WorldCount.Parameters
{
    public class WordCountSummaryResponse
    {
        private readonly ISet<string> _words = new HashSet<string>();

        public WordCountSummaryResponse(IEnumerable<string> words, IDictionary<string, int> wordCount)
        {
            WordCount = wordCount;

            foreach (var word in words) _words.Add(word);
        }

        public IDictionary<string, int> WordCount { get; }
    }
}