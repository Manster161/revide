﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WordCount.Core;
using WordCount.Core.Interfaces;
using WordCount.Core.Parameters;
using WordCount.Infrastructure.SearchProviders.Entities.Google;

namespace WordCount.Infrastructure.SearchProviders
{
    public class GoogleCustomSearchProvider : ISearchProvider
    {
        private readonly IHttpHelper _httpHelper;
        private const string cx = "006712954429953426046:nrel_6ylhue";
        private const string key = "AIzaSyARo4-miMhECAYt-eX55RlUPm-JFinJfUM";
        public string Name => "Google";

        public  GoogleCustomSearchProvider(IHttpHelper httpHelper)
        {
            _httpHelper = httpHelper;
        }

        public async Task<WordCountResult> CountWordAsync(WordCountQuery word)
        {
            var uriQuery = $"https://www.googleapis.com/customsearch/v1?q={word}&cx={cx}&key={key}";

            var result = await _httpHelper.Get<RootObject>(uriQuery);

            return new WordCountResult(Name, word.Word, int.Parse(result.searchInformation.totalResults), true);
        }
    }
}