﻿namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class SearchInformation
    {
        public double searchTime { get; set; }
        public string formattedSearchTime { get; set; }
        public string totalResults { get; set; }
        public string formattedTotalResults { get; set; }
    }
}