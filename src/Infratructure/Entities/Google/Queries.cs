﻿using System.Collections.Generic;

namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Queries
    {
        public List<Request> request { get; set; }
        public List<NextPage> nextPage { get; set; }
    }
}