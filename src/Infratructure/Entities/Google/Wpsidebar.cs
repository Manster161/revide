﻿using System;

namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Wpsidebar
    {
        public DateTime datepublished { get; set; }
        public string image { get; set; }
        public string url { get; set; }
    }
}