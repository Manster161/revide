﻿using System;

namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Comment
    {
        public DateTime datepublished { get; set; }
        public string url { get; set; }
        public string text { get; set; }
    }
}