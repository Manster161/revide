﻿namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Wpheader
    {
        public string headline { get; set; }
        public string description { get; set; }
    }
}