﻿namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Imageobject
    {
        public string url { get; set; }
        public string width { get; set; }
        public string height { get; set; }
        public string caption { get; set; }
    }
}