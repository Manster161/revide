﻿namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Article
    {
        public string mainentityofpage { get; set; }
        public string headline { get; set; }
        public string name { get; set; }
        public string datepublished { get; set; }
        public string articlebody { get; set; }
    }
}