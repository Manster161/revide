﻿namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class CseThumbnail
    {
        public string width { get; set; }
        public string height { get; set; }
        public string src { get; set; }
    }
}