﻿namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Sitenavigationelement
    {
        public string url { get; set; }
        public string name { get; set; }
    }
}