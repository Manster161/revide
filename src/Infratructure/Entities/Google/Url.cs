﻿using System.Text;

namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Url
    {
        public string type { get; set; }
        public string template { get; set; }
    }
}
