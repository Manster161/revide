﻿namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Spelling
    {
        public string correctedQuery { get; set; }
        public string htmlCorrectedQuery { get; set; }
    }
}