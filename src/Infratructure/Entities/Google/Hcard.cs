﻿namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Hcard
    {
        public string fn { get; set; }
        public string nickname { get; set; }
        public string photo { get; set; }
    }
}