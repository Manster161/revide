﻿namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Person
    {
        public string url { get; set; }
        public string name { get; set; }
        public string jobtitle { get; set; }
        public string description { get; set; }
    }
}