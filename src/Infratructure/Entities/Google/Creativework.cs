﻿using System;

namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Creativework
    {
        public string headline { get; set; }
        public DateTime datepublished { get; set; }
        public string text { get; set; }
    }
}