﻿using System.Collections.Generic;

namespace WordCount.Infrastructure.SearchProviders.Entities.Google
{
    public class Pagemap
    {
        public List<CseThumbnail> cse_thumbnail { get; set; }
 
        public List<Creativework> creativework { get; set; }
        public List<Sitenavigationelement> sitenavigationelement { get; set; }
        public List<Person> person { get; set; }
        public List<Comment> comment { get; set; }
        public List<CseImage> cse_image { get; set; }
        public List<Wpheader> wpheader { get; set; }

        public List<Article> article { get; set; }
        public List<Imageobject> imageobject { get; set; }
        public List<Organization> organization { get; set; }
        public List<Wpsidebar> wpsidebar { get; set; }
        public List<Webpage> webpage { get; set; }
        public List<Hcard> hcard { get; set; }
    }
}