﻿namespace WordCount.Infrastructure.SearchProviders.Entities.Bing
{
    public class RootObject
    {
        public string _type { get; set; }
        public QueryContext queryContext { get; set; }
        public WebPages webPages { get; set; }
        public RankingResponse rankingResponse { get; set; }
    }
}