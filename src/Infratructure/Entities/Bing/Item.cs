﻿namespace WordCount.Infrastructure.SearchProviders.Entities.Bing
{
    public class Item
    {
        public string answerType { get; set; }
        public int resultIndex { get; set; }
        public Value2 value { get; set; }
    }
}