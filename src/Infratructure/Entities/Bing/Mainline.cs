﻿using System.Collections.Generic;

namespace WordCount.Infrastructure.SearchProviders.Entities.Bing
{
    public class Mainline
    {
        public List<Item> items { get; set; }
    }
}