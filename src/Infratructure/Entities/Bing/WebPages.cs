﻿using System.Collections.Generic;

namespace WordCount.Infrastructure.SearchProviders.Entities.Bing
{
    public class WebPages
    {
        public string webSearchUrl { get; set; }
        public int totalEstimatedMatches { get; set; }
        public List<Value> value { get; set; }
        public bool someResultsRemoved { get; set; }
    }
}