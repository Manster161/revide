﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WordCount.Core;
using WordCount.Core.Interfaces;
using WordCount.Core.Parameters;
using WordCount.Infrastructure.SearchProviders.Entities.Bing;
using WorldCount.Utils;

namespace WordCount.Infrastructure.SearchProviders
{
    public class BingSearchProvider : ISearchProvider
    {
        private readonly IHttpHelper _httpHelper;
        private const string AccessKey = "14a271bd59144b29a731a16a40277863";
        private const string UriBase = "https://northeurope.api.cognitive.microsoft.com/bing/v7.0/search?q=";
        public string Name => "Bing";
        private readonly Dictionary<string, string> headers;

        public BingSearchProvider(IHttpHelper httpHelper)
        {
            _httpHelper = Guard.AgainstNull(httpHelper, nameof(httpHelper));
            headers = new Dictionary<string, string> {{"Ocp-Apim-Subscription-Key", AccessKey}};
        }

        public async Task<WordCountResult> CountWordAsync(WordCountQuery word)
        {
            var uriQuery = UriBase + Uri.EscapeDataString(word.Word);

            var root = await _httpHelper.Get<RootObject>(uriQuery, headers);

            return new WordCountResult(Name, root.queryContext.originalQuery,
                root.webPages.totalEstimatedMatches);
        }
    }
}