﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WordCount.Core.Interfaces;

namespace WordCount.Infrastructure.SearchProviders
{
    public class HttpHelper : IHttpHelper
    {
        public async Task<T> Get<T>(string uri, IDictionary<string, string> headers = null)
        {
            if (!Uri.IsWellFormedUriString(uri, UriKind.RelativeOrAbsolute))
                throw new ArgumentException($"{uri} is not a valid URI");

            var request = WebRequest.Create(uri);

            if (headers != null)
                foreach (var headersKey in headers.Keys)
                    request.Headers[headersKey] = headers[headersKey];


            var response = await request.GetResponseAsync();
            using (var sr = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException()))
            {
                var data = await sr.ReadToEndAsync();

                return JsonConvert.DeserializeObject<T>(data);
            }
        }
    }
}