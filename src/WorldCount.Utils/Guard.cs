﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace WorldCount.Utils
{
    public static class Guard
    {
        public static void Against<TException>(bool condition, string message) where TException : Exception
        {
            if (condition)
            {
                throw (TException)Activator.CreateInstance(typeof(TException), message);
            }
        }

        public static void Against<TException>(bool condition, string message, params object[] args)
            where TException : Exception
        {
            Against<TException>(condition, string.Format(CultureInfo.InvariantCulture, message, args));
        }

        public static void Against<TException>(Func<bool> condition, string message) where TException : Exception
        {
            if (condition())
            {
                throw (TException)Activator.CreateInstance(typeof(TException), message);
            }
        }

        public static void Against<TException>(Func<bool> condition, string message, params object[] args)
            where TException : Exception
        {
            Against<TException>(condition, string.Format(CultureInfo.InvariantCulture, message, args));
        }

        [DebuggerHidden]
        public static T AgainstNull<T>(T value, string paramName) where T : class
        {
            if (value == null)
            {
                throw new ArgumentNullException(paramName);
            }

            return value;
        }

        [DebuggerHidden]
        public static string AgainstNullOrEmpty(string value, string paramName)
        {
            AgainstNull(value, paramName);
            if (value == string.Empty)
            {
                throw new ArgumentException("Argument must not be the empty string", paramName);
            }

            return value;
        }
    }
}