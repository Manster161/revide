﻿namespace WordCount.Core.Parameters
{
    public class WordCountResult
    {
        public bool Success { get; }
        public string Provider { get; }    
        public string Word { get; }
        public int Count { get; }

        public WordCountResult(string provider, string word, int count, bool isSuccess = true)
        {
            Provider = provider;
            Word = word;
            Count = count;
            Success = isSuccess;
        }
    }
}