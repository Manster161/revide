﻿using System;

namespace WordCount.Core.Parameters
{
    public class WordCountQuery
    {
        public WordCountQuery(string word)
        {
            if (word.Split(' ').Length > 1)
            {
                throw new ArgumentException("Search word cannot contain whitespace");
            }

            if (string.IsNullOrEmpty(word))
            {
                throw new ArgumentException("Search word cannot be empty");
            }

            Word = word;
        }

        public string Word { get; }
    }
}
