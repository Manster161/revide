﻿using System.Collections.Generic;

namespace WordCount.Core
{
    public class WordCountResponse
    {
        public WordCountResponse()
        {
            Results = new List<KeyValuePair<string, int>>();
        }
        public IReadOnlyList<KeyValuePair<string, int>> Results { get; }

    }
}