﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WordCount.Core.Interfaces
{
    public interface IHttpHelper
    {
        Task<T> Get<T>(string uri, IDictionary<string, string> headers = null);
    }
}