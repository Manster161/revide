﻿using System.Threading.Tasks;
using WordCount.Core.Parameters;

namespace WordCount.Core.Interfaces
{
    public interface ISearchProvider
    {
        string Name { get; }
        Task<WordCountResult> CountWordAsync(WordCountQuery word);
    }
}